��             +         �     �     �  4   �     !     A  '   \     �     �     �     �     �  !   �  !     %   :     `  /   s     �     �     �  -   �          2     R     c     �  
   �     �     �     �     �  <     S  N     �      �  I   �  7   -	  ,   e	  A   �	  :   �	  D   
  ?   T
     �
  +   �
  L   �
  L     J   c  "   �  4   �  +        2  G   R  D   �  2   �  /        B  G   b  E   �     �       .   (  /   W     �  s   �                                
                                          	                                                                      Canceled payment Canceled payment. Cancellation of payment at the request of the buyer. Choose language of payment page ConcordPay Payment Gateway Error: This order has already been paid Error: Unknown operation type Error: Wrong payment signature Given to Merchant by ConcordPay Language Merchant ID Order status after failed payment Order status after refund payment Order status after successful payment Pay via ConcordPay Payment Visa, Mastercard, Apple Pay, Google Pay Payment by card on the site Payment declined Payment failed order status Payment have been refunded through ConcordPay Payment processed fail. Payment processed successfully. Payment refunded Payment refunded order status Payment successful order status Secret key Successful payment Thank you, payment approved. Unfortunately payment declined. Unsuccessful payment You can find a list of your orders in your personal account. Project-Id-Version: 
PO-Revision-Date: 2021-09-22 16:40+0300
Last-Translator: 
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: t
X-Poedit-SearchPath-0: uc_concordpay.info
X-Poedit-SearchPath-1: uc_concordpay.module
X-Poedit-SearchPath-2: uc_concordpay.pages.inc
X-Poedit-SearchPath-3: class/concordpay.php
 Скасована оплата Скасовано платіж. Скасування платежу за бажанням покупця. Виберіть мову сторінки оплати ConcordPay - платіжна система Помилка: це замовлення вже оплачено Помилка: невідомий тип операції Помилка: неправильний підпис платежу Надається продавцю системою ConcordPay Мова Ідентифікатор продавця Статус замовлення при відхиленні платежу Статус замовлення при поверненні платежу Статус замовлення після успішної оплати Оплата через ConcordPay Оплата Visa, Mastercard, Apple Pay, Google Pay Оплата карткою на сайті Оплата відхилена Статус замовлення відхиленого платежу Платіж повернено через систему ConcordPay Не вдалося обробити платіж. Платіж успішно оброблено. Платіж повернено Статус замовлення повернутого платежу Статус успішно оплаченого замовлення Секретний ключ Успішна оплата Дякуємо, платіж схвалено. На жаль, оплата відхилена. Неуспішна оплата Список ваших замовлень ви можете знайти в особистому кабінеті. 