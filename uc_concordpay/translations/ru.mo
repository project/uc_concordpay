��    !      $  /   ,      �     �     �  4        A     a  '   |     �     �     �          
  !     !   8  %   Z     �  /   �     �     �     �  -        :     V     n     �     �     �  
   �     �     �          8  <   M  c  �  !   �     	  E   -	  7   s	  .   �	  7   �	  <   
  <   O
  =   �
     �
  +   �
  @   �
  D   @  F   �  "   �  4   �  )   $     N  ?   l  <   �  3   �       /   <     l  C   �  ;   �          (  .   F  5   u  !   �  e   �                                                             !                                          
       	                                                 Canceled payment Canceled payment. Cancellation of payment at the request of the buyer. Choose language of payment page ConcordPay Payment Gateway Error: This order has already been paid Error: Unknown operation type Error: Wrong payment signature Given to Merchant by ConcordPay Language Merchant ID Order status after failed payment Order status after refund payment Order status after successful payment Pay via ConcordPay Payment Visa, Mastercard, Apple Pay, Google Pay Payment by card on the site Payment declined Payment failed order status Payment have been refunded through ConcordPay Payment made via ConcordPay Payment processed fail. Payment processed successfully. Payment refunded Payment refunded order status Payment successful order status Secret key Successful payment Thank you, payment approved. Unfortunately payment declined. Unsuccessful payment You can find a list of your orders in your personal account. Project-Id-Version: 
PO-Revision-Date: 2021-09-22 16:38+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-KeywordsList: __;_e;translate;t
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: uc_concordpay.module
X-Poedit-SearchPath-1: uc_concordpay.pages.inc
X-Poedit-SearchPath-2: uc_concordpay.info
X-Poedit-SearchPath-3: class/concordpay.php
 Отмененный платеж Платеж отменен. Отмена платежа по желанию покупателя. Выберите язык страницы оплаты ConcordPay - платежная система Ошибка: Этот заказ уже оплачен Ошибка: Неизвестный тип операции Ошибка: Неверная подпись платежа Выдаётся продавцу системой ConcordPay Язык Идентификатор продавца Статус заказа после отмены платежа Статус заказа после возврата платежа Статус заказа после успешного платежа Оплата через ConcordPay Оплата Visa, Mastercard, Apple Pay, Google Pay Оплата картой на сайте Платеж отклонен Статус заказа отмененного платежа Платеж был возвращен через ConcordPay Платеж совершен через ConcordPay Платеж отклонен. Платеж успешно обработан. Платёж возвращён Статус заказа возвращенного платежа Статус заказа успешного платежа Секретный ключ Успешный платеж Спасибо, оплата одобрена. К сожалению, платеж отклонен. Неуспешный платеж Список ваших заказов вы можете найти в личном кабинете. 